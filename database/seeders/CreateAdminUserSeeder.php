<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Hardik Savani', 
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456')
        ]);
    
        $role = Role::create(['name' => 'Admin']);//creame un rol con el nombre admin
     
        $permissions = Permission::pluck('id','id')->all();//sacando todos los permisos
   
        $role->syncPermissions($permissions);//se le esta asigando a este rol
     
        $user->assignRole([$role->id]);//asignamos el rol al usuario
    }
}
