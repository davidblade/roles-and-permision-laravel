@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}

                    <ul>
                        @can('role-list')
                            <li>hola mundo</li>
                        @endcan

                        @can('product-blade')
                            <li>hola mundo2</li>
                        @endcan
                        <li>hola mundo3</li>
                        @role('invitado')
                            <li>pepito</li>
                        @endrole
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
